package com.example.leandro.checkymail;

import android.content.Context;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.widget.Toast;

/**
 * Created by Leandro on 6/20/2015.
 */
public class Notifications extends ContextWrapper {
    public Notifications() {
        super(null);
    }

    public Notifications(Context base) {
        super(base);
    }

    public void showInStatusbar(String tickerText) {
        showInStatusbar(tickerText, false, false, false);
    }

    public void showInStatusbar(String tickerText, boolean alert, boolean vibrate, boolean lights) {
        String svcName = Context.NOTIFICATION_SERVICE;
        NotificationManager notificationManager = (NotificationManager) getSystemService(svcName);

        int icon = R.mipmap.ic_launcher;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, tickerText, when);

        // activity to launch when clicked
        Context context = getApplicationContext();
        Intent intent = new Intent(this, CheckyMailActivity.class);
        PendingIntent launchIntent = PendingIntent.getActivity(context, 0, intent, 0);
        notification.setLatestEventInfo(context, "CheckyMail", tickerText, launchIntent);

        // sounds, lights and vibration
        if (alert) {
            notification.defaults = Notification.DEFAULT_SOUND;
            if (vibrate) {
                notification.defaults = notification.defaults | Notification.DEFAULT_VIBRATE;
            }
            if (lights) {
                notification.ledARGB = Color.GREEN;
                notification.ledOffMS = 0;
                notification.ledOnMS = 1;
                notification.flags = notification.flags | Notification.FLAG_SHOW_LIGHTS;
            }
        }

        // auto-cancel notif
        notification.flags = notification.flags | Notification.FLAG_AUTO_CANCEL;

        int NOTIFICATION_REF = 1;
        notificationManager.notify(NOTIFICATION_REF, notification);
    }

    public void showToast(String msg) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }

}
