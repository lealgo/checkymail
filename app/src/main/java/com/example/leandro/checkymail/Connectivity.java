package com.example.leandro.checkymail;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Leandro on 6/20/2015.
 */
public class Connectivity extends ContextWrapper {

    public Connectivity(Context base) {
        super(base);
    }

    public boolean isDataEnabled() {
        String service = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(service);
        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    public boolean isConnected() {
        String service = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(service);
        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public boolean isConnectedOrConnecting() {
        String service = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(service);
        NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public void enableData(boolean enabled) {
        try {
            String service = Context.CONNECTIVITY_SERVICE;
            ConnectivityManager dataManager = (ConnectivityManager) getSystemService(service);
            Method dataMethod = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
            dataMethod.setAccessible(true);
            dataMethod.invoke(dataManager, enabled);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public boolean isWifiEnabled() {
        String service = Context.WIFI_SERVICE;
        WifiManager wifi = (WifiManager) getSystemService(service);
        return wifi.isWifiEnabled();
    }

    public void enableWifi(boolean enabled) {
        String service = Context.WIFI_SERVICE;
        WifiManager wifi = (WifiManager) getSystemService(service);
        wifi.setWifiEnabled(enabled);
    }

    public boolean isAirplaneModeEnabled() {
        return Settings.System.getInt(getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
    }

    public void enableAirplaneMode(boolean enabled) {
        // toggle airplane mode
        Settings.System.putInt(getContentResolver(), Settings.System.AIRPLANE_MODE_ON, enabled ? 1 : 0);
        // Post an intent to reload
        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intent.putExtra("state", enabled);
        sendBroadcast(intent);
    }
}
