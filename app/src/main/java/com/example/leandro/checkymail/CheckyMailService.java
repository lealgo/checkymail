package com.example.leandro.checkymail;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import java.io.IOException;
import java.util.Date;


public class CheckyMailService extends IntentService {

    private static boolean alert = false;

    public CheckyMailService() {
        super("CheckyMailService");
    }

    public CheckyMailService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // get preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String host_name = prefs.getString(CheckyMailActivity.PREF_HOST_NAME, "");
        String user_name = prefs.getString(CheckyMailActivity.PREF_USER_NAME, "");
        String password = prefs.getString(CheckyMailActivity.PREF_PASSWORD, "");
        int timeout = Integer.parseInt(prefs.getString(CheckyMailActivity.PREF_TIMEOUT, "0"));
        boolean sync_over_wifi = prefs.getBoolean(CheckyMailActivity.PREF_SYNC_OVER_WIFI, false);
        boolean sync_sleep_at_night = prefs.getBoolean(CheckyMailActivity.PREF_SYNC_SLEEP_AT_NIGHT, true);
        boolean notify = prefs.getBoolean(CheckyMailActivity.PREF_NOTIFY_NEW_MESSAGE, true);
        boolean vibrate = prefs.getBoolean(CheckyMailActivity.PREF_NOTIFY_VIBRATE, true);
        boolean lights = prefs.getBoolean(CheckyMailActivity.PREF_NOTIFY_LIGHTS, true);
        boolean handleConnection = true;
        // check sleep at night
        if (!sync_sleep_at_night || !isNight()) {
            Connectivity connectivity = new Connectivity(getBaseContext());
            boolean wifiWasEnabled = false;
            boolean dataWasEnabled = false;
            if (handleConnection) {
                // get connectivity status
                wifiWasEnabled = connectivity.isWifiEnabled();
                dataWasEnabled = connectivity.isDataEnabled();
                // setup connections
                connectivity.enableWifi(sync_over_wifi);
                connectivity.enableData(!sync_over_wifi);
                // wait for the connection to settle
                wait(5 * 1000);
                // try to reconnect to the network
                if (!sync_over_wifi && !connectivity.isConnectedOrConnecting()) {
                    connectivity.enableAirplaneMode(true);
                    wait(5 * 1000);
                    connectivity.enableAirplaneMode(false);
                    wait(15 * 1000);
                }
            }
            // only check email if connected
            int emailCount = -1;
            String message = null;
            if (connectivity.isConnectedOrConnecting()) {
                // check email
                try {
                    emailCount = getEmailCount(host_name, user_name, password, timeout * 1000);
                    message = String.format("You've got %s new email%s", emailCount == 0 ? "no" : emailCount, emailCount == 1 ? "" : "s");
                } catch (Exception e) {
                    emailCount = -1;
                    message = e.getMessage();
                    e.printStackTrace();
                }
            }
            else {
                message = "Could not connect to the network. Not checking this time.";
            }
            if (handleConnection) {
                // restore connections
                connectivity.enableData(dataWasEnabled);
                connectivity.enableWifi(wifiWasEnabled);
            }
            // show message only if the check succeeded, or there wasn't a previous alert
            boolean showMessage = emailCount >= 0 || !alert;
            // alert only if it's set in preferences, and there are new emails
            alert = notify && emailCount > 0;
            // notify
            if (showMessage) {
                new Notifications(getApplicationContext()).showInStatusbar(message, alert, vibrate, lights);
            }
        } else {
            // TODO: should cancel alarm
        }
    }

    private boolean isNight() {
        return new Date().getHours() < 8;
    }

    private int getEmailCount(String host, String user_name, String password, int timeout) throws IOException {
        POP3Client client = new POP3Client();
        client.setTimeout(timeout);
        client.setDebug(true);
        client.connect(host);
        client.login(user_name, password);
        int newEmails = client.getNumberOfNewMessages();
        client.logout();
        client.disconnect();
        return newEmails;
    }

    private void wait(int timeout) {
        if (timeout > 0) {
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
