package com.example.leandro.checkymail;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by Leandro on 6/22/2015.
 */
public class POP3Client {

    private Socket socket;

    private int timeout = 0;

    private boolean debug = false;

    private BufferedReader reader;
    private BufferedWriter writer;

    private static final int DEFAULT_PORT = 110;

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void connect(String host, int port) throws IOException {
        if (debug)
            Log.d(CheckyMailActivity.LOG_TAG, "timeout: " + timeout);
        socket = new Socket();
        socket.setSoTimeout(timeout);
        socket.connect(new InetSocketAddress(host, port), timeout);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        if (debug)
            Log.d(CheckyMailActivity.LOG_TAG, "Connected to the host");
        readResponseLine();
    }

    public void connect(String host) throws IOException {
        connect(host, DEFAULT_PORT);
    }

    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public void disconnect() throws IOException {
        if (!isConnected())
            throw new IllegalStateException("Not connected to a host");
        socket.close();
        reader = null;
        writer = null;
        if (debug)
            Log.d(CheckyMailActivity.LOG_TAG, "Disconnected from the host");
    }

    protected String readResponseLine() throws IOException {
        String response = reader.readLine();
        if (debug) {
            Log.d(CheckyMailActivity.LOG_TAG, "DEBUG [in] : " + response);
        }
        if (response.startsWith("-ERR"))
            throw new RuntimeException("Server has returned an error: " + response.replaceFirst("-ERR ", ""));
        return response;
    }

    protected String sendCommand(String command) throws IOException {
        if (debug) {
            Log.d(CheckyMailActivity.LOG_TAG, "DEBUG [out]: " + command);
        }
        writer.write(command + "\n");
        writer.flush();
        return readResponseLine();
    }

    public void login(String username, String password) throws IOException {
        sendCommand("USER " + username);
        sendCommand("PASS " + password);
    }

    public void logout() throws IOException {
        sendCommand("QUIT");
    }

    public int getNumberOfNewMessages() throws IOException {
        String response = sendCommand("STAT");
        String[] values = response.split(" ");
        return Integer.parseInt(values[1]);
    }


}

