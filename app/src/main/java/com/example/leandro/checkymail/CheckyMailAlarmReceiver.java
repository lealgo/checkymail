package com.example.leandro.checkymail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Leandro on 6/20/2015.
 */
public class CheckyMailAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startIntent = new Intent(context, CheckyMailService.class);
        context.startService(startIntent);
    }

    public static final String ACTION_CHECKYMAIL_ALARM = "com.example.leandro.checkymail.ACTION_CHECKYMAIL_ALARM";
}